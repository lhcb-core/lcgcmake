#!/usr/bin/env python
from __future__ import print_function

def read_buildinfo(line):
    # fix line
    if line[0] == " ":
        line = line[1:]
    if line[-1] == "\n":
        line = line[:-1]
    if not line or line.startswith("#"): 
        return
    pairs = [ i.split(":", 1) for i in line.split(", ") if i]
    info = dict([(k, v[1:]) for k, v in pairs]) # v - remove space in front
    info["DEPENDS"] = [ v for v in info["DEPENDS"].split(",") if v  ]
    info["DEPENDENCIES"] = [ v.split("-") for v in info["DEPENDS"] ]
    info["DEPENDSDICT"] = [ dict(zip(["NAME", "VERSION", "HASH"], v.split("-"))) for v in info["DEPENDS"] ]
    return info


def read_buildinfo_file(filename):
    if filename.endswith("*"):
        import os
        directory = filename[:-1]
        files = os.listdir(directory)
        buildinfofile = [ f for f in files if f.startswith(".buildinfo") and f.endswith(".txt") ][0]
        filename = os.path.join(directory, buildinfofile)

    with open(filename) as f:
        info = []
        for line in f:
            if line.startswith("#"): continue
            buildinfo = read_buildinfo(line)
            if buildinfo:
                info.append(buildinfo)
        return info

def read_buildinfo_from_tarfile(filename):
    import tarfile
    f = tarfile.open(filename)
    info = []
    for m in f.getmembers():
        if ".buildinfo" in m.name:
            for line in f.extractfile(m):
                buildinfo = read_buildinfo(line)
                if buildinfo:
                    info.append(buildinfo)
    f.close()

    if info:
        return info
    else:        
        raise Exception("No buildinfo file inside tarfile")


if __name__ == "__main__":
    import sys
    import argparse
    parser = argparse.ArgumentParser(description="Print LCG_contrib_<platform>.txt file (buildinfo dump)")
    parser.add_argument("--pattern", type=str, help="Python .format() pattern, e.g.: '{NAME}-{VERSION}-{HASH}'")
    parser.add_argument("filenames", type=str, nargs="+", help="Concatenated buildinfo files created after LCGCMake finishes compilation")

    args = parser.parse_args()
    for f in args.filenames:
        if f.endswith(".tgz"):
            infolist = read_buildinfo_from_tarfile(f)
        else:
            infolist = read_buildinfo_file(f)
        if args.pattern:
            for item in infolist:
                print(args.pattern.format(**item))
        else:
            for item in infolist:
                for k, v in item.items():
                    if "DEPEND" not in k:
                        print("{}: {}".format(k, v)) 
                print("DEPENDS: {}".format(', '.join(item["DEPENDS"])))
                print("")
