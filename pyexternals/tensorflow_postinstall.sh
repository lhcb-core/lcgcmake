#!/usr/bin/env bash
# Link include files from tensorflow python package to $INSTALLPATH/include/
# tensorflow_postintall.sh <INSTALL_DIR>
find lib/python*/site-packages/tensorflow/include -maxdepth 1 -mindepth 1 \
    -exec bash -c 'cd $1/include/tensorflow_externals && ln -s ../../$2 $(basename $2)' sh $1 {} \;

